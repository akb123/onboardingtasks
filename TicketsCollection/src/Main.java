/**
 * Created by beheraa on 8/24/2017.
 */
public class Main {

    public static void main(String[] args) throws Exception {

        TicketOperations tickOper = new TicketActualOperations();
        Ticket ticket = new TicketClass("21/09/2017","hyderabad","rourkela", "15:50",346.00);
        Ticket ticket1 = new TicketClass("12/11/2017","delhi","ranchi", "17:10",240.00);
        tickOper.addTicket(ticket);
        tickOper.addTicket(ticket1);
        tickOper.printTickets();
        tickOper.removeTicket(ticket);

    }
}

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by beheraa on 8/24/2017.
 */
public class TicketActualOperations implements TicketOperations {

    ArrayList<Ticket> ticketArrayList= new ArrayList<>();

    @Override
    public boolean addTicket(Ticket ticket) {
        ticketArrayList.add(ticket);
        System.out.println("Ticket Added Successfully");
        System.out.println(ticket);
        return true;
    }

    @Override
    public Ticket removeTicket(Ticket ticket) {
        if(!ticketArrayList.isEmpty()) {
            ticketArrayList.remove(ticket);
            System.out.println("Removed Ticket Sucessfully");
            System.out.println(ticket);
            return ticket;
        } else {
            System.out.println("Trying to remove from empty list");
            return null;
        }
    }

    @Override
    public ArrayList<Ticket> printTickets() {
        Iterator iterator = ticketArrayList.iterator();
        if (ticketArrayList.isEmpty()) {
            System.out.println("Ticket List is empty!");
            return null;
        } else {
            System.out.println("Printing tickets");
            while (iterator.hasNext()) {
                Ticket ticket = (TicketClass) iterator.next();
                System.out.println(ticket.dateOfJourney() + "\n" + ticket.boardingPlace() + "\n" + ticket.destinationPlace() + "\n" + ticket.time() + ticket.price());
            }
            return ticketArrayList;
        }
    }


}

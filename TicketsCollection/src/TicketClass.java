/**
 * Created by beheraa on 8/24/2017.
 */
public class TicketClass implements Ticket {

    private String dateOfJourney;
    private String boardingPlace;
    private String destination;
    private String time;
    private double price;

    public TicketClass(String dateOfJourney, String boardingPlace, String destination, String time, double price) {
        this.dateOfJourney = dateOfJourney;
        this.boardingPlace = boardingPlace;
        this.destination = destination;
        this.time = time;
        this.price = price;
    }


    @Override
    public String dateOfJourney() {
        return dateOfJourney;
    }

    @Override
    public String boardingPlace() {
        return boardingPlace;
    }

    @Override
    public String destinationPlace() {
        return destination;
    }

    @Override
    public String time() {
        return time;
    }

    @Override
    public double price() {
        return price;
    }

//    public String toString() {
//        return "Date of Journey:" + this.dateOfJourney + "\n Boarding place:" + this.boardingPlace() + "\n Destination:" + this.destinationPlace() + "\n Time:" + this.time() + "\n Price" + this.price;
//    }
}

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by beheraa on 8/24/2017.
 */
public interface TicketOperations {

    public boolean addTicket(Ticket ticket);
    public Ticket removeTicket(Ticket ticket);
    public ArrayList<Ticket> printTickets();

}

/**
 * Created by beheraa on 8/24/2017.
 */
public interface Ticket {

    public String dateOfJourney();
    public String boardingPlace();
    public String destinationPlace();
    public String time();
    public double price();
}

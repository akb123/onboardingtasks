import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;
/**
 * Created by beheraa on 8/24/2017.
 */
public class TicketOperationsTest {

    @Test
    public void addTheTicketsSuccesfully() {

        Ticket ticket = new TicketClass("21/09/2017","hyderabad","rourkela", "15:50",346.00);
        Ticket ticket1 = new TicketClass("12/11/2017","delhi","ranchi", "17:10",240.00);
        TicketOperations tickOper = new TicketActualOperations();
        assertEquals(true,tickOper.addTicket(ticket));

    }

    @Test
    public void printTicketList() {
        Ticket ticket = new TicketClass("21/09/2017","hyderabad","rourkela", "15:50",346.00);
        Ticket ticket1 = new TicketClass("12/11/2017","delhi","ranchi", "17:10",240.00);
        TicketOperations tickOper = new TicketActualOperations();
        ArrayList<Ticket> ticketArrayList = new ArrayList<>();
        ticketArrayList.add(ticket);
        ticketArrayList.add(ticket1);
        tickOper.addTicket(ticket);
        tickOper.addTicket(ticket1);
        assertEquals(ticketArrayList,tickOper.printTickets());
    }

    @Test
    public void returnNullWhenPrintingEmptyTicketList() {
        Ticket ticket = new TicketClass("21/09/2017","hyderabad","rourkela", "15:50",346.00);
        TicketOperations tickOper = new TicketActualOperations();
        assertEquals(null,tickOper.printTickets());
    }

    @Test
    public void removeTicketSuccessfullyWhenTicketsArePresent() {
        Ticket ticket = new TicketClass("21/09/2017","hyderabad","rourkela", "15:50",346.00);
        Ticket ticket1 = new TicketClass("12/11/2017","delhi","ranchi", "17:10",240.00);
        TicketOperations tickOper = new TicketActualOperations();
//        ArrayList<Ticket> ticketArrayList = new ArrayList<>();
//        ticketArrayList.add(ticket);
//        ticketArrayList.add(ticket1);
        tickOper.addTicket(ticket);
        tickOper.addTicket(ticket1);
        assertEquals(ticket,tickOper.removeTicket(ticket));
    }

    @Test
    public void returnNullWhenTicketsAreRemovedFromEmptyList() {
        Ticket ticket = new TicketClass("21/09/2017","hyderabad","rourkela", "15:50",346.00);
        TicketOperations tickOper = new TicketActualOperations();
        assertEquals(null,tickOper.removeTicket(ticket));

    }
}
